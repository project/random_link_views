# Random link from views

Adds the ability to generate a set of paths with views and use it
to randomize a link using that set of paths.
The results of the view are passed to javascript to do the randomization.

# Configuration

Create a view with a "Random link" display and use fields for the format.
Add a field that displays just the string of the path that goes into the href
of the link with no HTML markup.

Configure that field as the "Replacement" in the Random link settings.
The attribute to modify is the href by default and the selector
for any link that points to "#random" is also a default.

Then the view can be configured according to the requirements like
only random blog posts. Remove any sort criteria or use
the "Global: random" any result from the set is a candidate to go
into the random draw.

Save the view and go to the block configuration for a menu.

In a block configuration form for a menu the view that was configured can be
seen under the "Random link views" section. Enable it and save the block.

Then create a menu item in the menu corresponding to the block that has
the configuration enabled linking to the fragment #random.

When displaying that menu the href for that link should be replaced
for one of the paths that the view returns.

# Flexibility

The defaults cover the case for a random link but since the selector and the attribute
for the replacement can also be configured in views and randomized there is more
possibilities than just random links.
