<?php

namespace Drupal\random_link_views\Plugin\views\display;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The plugin that handles an random link display in views.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "random_link",
 *   title = @Translation("Random link"),
 *   admin = @Translation("Random link"),
 *   help = @Translation("Create the set to pick for random draws"),
 *   uses_menu_links = FALSE,
 *   theme = "views_view",
 * )
 */
class RandomLink extends DisplayPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Whether the display allows attachments.
   *
   * @var bool
   *   TRUE if the display can use attachments, or FALSE otherwise.
   */
  protected $usesAttachments = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct([], $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['selector']['default'] = 'a[href="#random"]';
    $options['selector_field']['default'] = '';
    $options['attribute']['default'] = 'href';
    $options['attribute_field']['default'] = '';
    $options['replacement']['default'] = 'Random';
    $options['replacement_field']['default'] = '';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    $categories['random_link'] = [
      'title' => $this->t('Random link settings'),
      'column' => 'second',
      'build' => [
        '#weight' => -10,
      ],
    ];

    $options['selector'] = [
      'category' => 'random_link',
      'title' => $this->t('Selector'),
      'value' => $this->getOption('selector') ?: $this->t('Field (@field)', [
        '@field' => $this->getFieldLabels()[$this->getOption('selector_field')] ?? $this->t('Broken'),
      ]),
    ];

    $options['attribute'] = [
      'category' => 'random_link',
      'title' => $this->t('Attribute name'),
      'value' => $this->getOption('attribute') ?: $this->t('Field (@field)', [
        '@field' => $this->getFieldLabels()[$this->getOption('attribute_field')] ?? $this->t('Broken'),
      ]),
    ];

    $options['replacement'] = [
      'category' => 'random_link',
      'title' => $this->t('Replacement'),
      'value' => $this->getOption('replacement') ?: $this->t('Field (@field)', [
        '@field' => $this->getFieldLabels()[$this->getOption('replacement_field')] ?? $this->t('Broken'),
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $formState) {
    parent::buildOptionsForm($form, $formState);
    $methodCall = 'buildOptionsForm' . ucfirst($formState->get('section'));
    if ($formState->get('section') && method_exists($this, $methodCall)) {
      $this->{$methodCall}($form, $formState);
    }
  }

  /**
   * Form for the selector configuration value.
   */
  protected function buildOptionsFormSelector(&$form, FormStateInterface $formState) {
    $fields = $this->getFieldLabels();
    $form['selector_field'] = [
      '#title' => $this->t('Selector field'),
      '#type' => 'select',
      '#options' => ['' => $this->t('Fixed value')] + $fields,
      '#default_value' => $this->getOption('selector_field'),
      '#description' => $this->t('The field that contains the CSS selector for the element that is going to have the attribute replaced.'),
    ];
    $form['selector'] = [
      '#title' => $this->t('Selector'),
      '#type' => 'textfield',
      '#default_value' => $this->getOption('selector'),
      '#description' => $this->t('Alternatively configure a static selector.'),
      '#states' => [
        'visible' => [
          ':input[name="selector_field"]' => ['value' => ''],
        ],
        'required' => [
          ':input[name="selector_field"]' => ['value' => ''],
        ],
      ],
    ];
  }

  /**
   * Form for the attribute configuration value.
   */
  protected function buildOptionsFormAttribute(&$form, FormStateInterface $formState) {
    $fields = $this->getFieldLabels();
    $form['attribute_field'] = [
      '#title' => $this->t('Attribute field'),
      '#type' => 'select',
      '#options' => ['' => $this->t('Fixed value')] + $fields,
      '#default_value' => $this->getOption('attribute_field'),
      '#description' => $this->t('The field that contains the attribute to be replaced in the element. For example href or innerHTML.'),
    ];
    $form['attribute'] = [
      '#title' => $this->t('Attribute name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->getOption('attribute'),
      '#description' => $this->t('Alternatively configure a static attribute.'),
      '#states' => [
        'visible' => [
          ':input[name="attribute_field"]' => ['value' => ''],
        ],
        'required' => [
          ':input[name="attribute_field"]' => ['value' => ''],
        ],
      ],
    ];
  }

  /**
   * Form for the replacement configuration value.
   */
  protected function buildOptionsFormReplacement(&$form, FormStateInterface $formState) {
    $fields = $this->getFieldLabels();
    $form['replacement_field'] = [
      '#title' => $this->t('Replacement field'),
      '#type' => 'select',
      '#options' => ['' => $this->t('Fixed value')] + $fields,
      '#default_value' => $this->getOption('replacement_field'),
      '#description' => $this->t('The field that contains the value replacement for the selected element by the selector configuration and for the attribute configured.'),
    ];
    $form['replacement'] = [
      '#title' => $this->t('Replacement'),
      '#type' => 'textfield',
      '#default_value' => $this->getOption('replacement'),
      '#description' => $this->t('Alternatively have a static replacement.'),
      '#states' => [
        'visible' => [
          ':input[name="replacement_field"]' => ['value' => ''],
        ],
        'required' => [
          ':input[name="replacement_field"]' => ['value' => ''],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $formState) {
    parent::submitOptionsForm($form, $formState);
    $methodCall = 'submitOptionsForm' . ucfirst($formState->get('section'));
    if ($formState->get('section') && method_exists($this, $methodCall)) {
      $this->{$methodCall}($form, $formState);
    }
  }

  /**
   * Submit handler for the selector options.
   */
  protected function submitOptionsFormSelector(&$form, FormStateInterface $formState) {
    $field = $formState->getValue('selector_field');
    if ($field) {
      $this->setOption('selector', '');
      $this->setOption('selector_field', $field);
    }
    else {
      $this->setOption('selector_field', '');
      $this->setOption('selector', $formState->getValue('selector'));
    }
  }

  /**
   * Submit handler for the attribute options.
   */
  protected function submitOptionsFormAttribute(&$form, FormStateInterface $formState) {
    $field = $formState->getValue('attribute_field');
    if ($field) {
      $this->setOption('attribute', '');
      $this->setOption('attribute_field', $field);
    }
    else {
      $this->setOption('attribute_field', '');
      $this->setOption('attribute', $formState->getValue('attribute'));
    }
  }

  /**
   * Submit handler for the replacement options.
   */
  protected function submitOptionsFormReplacement(&$form, FormStateInterface $formState) {
    $field = $formState->getValue('replacement_field');
    if ($field) {
      $this->setOption('replacement', '');
      $this->setOption('replacement_field', $field);
    }
    else {
      $this->setOption('replacement_field', '');
      $this->setOption('replacement', $formState->getValue('replacement'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function remove() {
    // Clean up block configs before the display disappears.
    $longname = $this->view->storage->get('id') . ':' . $this->display['id'];

    $blockStorage = $this
      ->entityTypeManager
      ->getStorage('block');
    foreach ($blockStorage->loadMultiple(
      $blockStorage
        ->getQuery()
        ->exists('third_party_settings.random_link_views')
        ->execute()
    ) as $block) {
      $setting = $block->getThirdPartySetting('random_link_views', 'views');
      unset($setting[$longname]);
      $block->setThirdPartySetting('random_link_views', 'views', $setting);
      $block->save();
    }

    parent::remove();
  }

}
