<?php

namespace Drupal\random_link_views\AutoEventSubscriber;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alter the form to be able to configure it.
 */
class BlockThirdPartySettings implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs event subscriber.
   *
   * @param  $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,  AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      // React on "block_form" form.
      'hook_event_dispatcher.form_block_form.alter' => 'alterBlockForm',
      HookEventDispatcherInterface::ENTITY_PRE_SAVE => 'entityPreSave',
    ];
  }

  /**
   * Add the form pieces for the new block settings.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent $event
   *   The event.
   */
  public function alterBlockForm(FormIdAlterEvent $event): void {
    if (!$this->currentUser->hasPermission('administer random link views')) {
      return;
    }
    $form = &$event->getForm();
    $formState = $event->getFormState();
    $block = $formState->getFormObject()->getEntity();

    $form['third_party_settings']['#tree'] = TRUE;
    $form['third_party_settings']['random_link_views'] = [
      '#type' => 'details',
      '#title' => $this->t('Random link views'),
    ];
    $viewStorage = $this
      ->entityTypeManager
      ->getStorage('view');
    $views = $viewStorage->loadMultiple(
      $viewStorage
        ->getQuery()
        ->condition('display.*.display_plugin', 'random_link')
        ->execute()
    );
    $viewsOptions = [];
    foreach ($views as $view) {
      $displays = $view->get('display');
      foreach ($displays as $display) {
        if ($display['display_plugin'] === 'random_link') {
          $viewsOptions[$view->id() . ':' . $display['id']] = $view->label() . ': ' . $display['display_title'];
        }
      }
    }
    if (count($viewsOptions) === 0) {
      $form['third_party_settings']['random_link_views']['views'] = [
        '#markup' => $this->t('There is no views configured with the a "Random link" display type'),
      ];
      return;
    }

    $form['third_party_settings']['random_link_views']['views'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Views for random link substitution'),
      '#default_value' => $block->getThirdPartySetting('random_link_views', 'views') ?? [],
      '#options' => $viewsOptions,
      '#description' => $this->t('Enabling a view will load the necessary javascript code for the random changes.'),
    ];
  }

  /**
   * Remove settings when empty.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent $event
   *   The event.
   */
  public function entityPreSave(EntityPresaveEvent $event): void {
    $entity = $event->getEntity();
    if ($entity instanceof ThirdPartySettingsInterface) {
      $setting = $entity->getThirdPartySetting('random_link_views', 'views');
      if (!$setting || count(array_filter($setting)) === 0) {
        $entity->unsetThirdPartySetting('random_link_views', 'views');
      }
    }
  }

}
