<?php

namespace Drupal\random_link_views\AutoEventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\block\Entity\Block;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\views\Views;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Apply actions event subscriber.
 */
class BlockAttach implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   Event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {
    /** @var \Drupal\preprocess_event_dispatcher\Variables\BlockEventVariables $variables */
    $variables = $event->getVariables();
    $elements = $variables->get('elements', []);

    // Blocks coming from page manager widget do not have id.
    if (!array_key_exists('#id', $elements)) {
      return;
    }
    $block = Block::load($elements['#id']);
    if ($block && $views = $block->getThirdPartySetting('random_link_views', 'views')) {
      $content = &$variables->getByReference('content');
      $views = array_filter($views);
      if (count($views) > 0) {
        $content['#attached']['library'][] = 'random_link_views/random_link_views';
      }
      foreach ($views as $view) {
        list($viewId, $displayId) = explode(':', $view);

        // Load the view.
        $view = Views::getView($viewId);
        $view->setDisplay($displayId);
        $view->execute();

        $rows = [
          'randomSet' => [],
          'selector' => $view->display_handler->getOption('selector'),
          'attribute' => $view->display_handler->getOption('attribute') ,
          'replacement' => $view->display_handler->getOption('replacement'),
        ];
        foreach ($view->result as $key => $result) {
          foreach (['selector', 'attribute', 'replacement'] as $configItem) {
            $field = $view->display_handler->getOption($configItem . '_field');
            if ($field) {
              $render = $view->field[$field]->render($result);
              $rows['randomSet'][$key][$configItem] = $render['#markup'] ?? '';
            }
          }
        }
        $content['#attached']['drupalSettings']['randomLinkViews'][] = $rows;

        // Extract some cache metadata from the view.
        // Ignore cache contexts as they would create too many variations.
        $viewRenderable = $view->buildRenderable();
        $content['#cache']['max-age'] = Cache::mergeMaxAges(
          $content['#cache']['max-age'] ?? Cache::PERMANENT,
          $viewRenderable['#cache']['max-age']
        );
        $content['#cache']['tags'] = Cache::mergeTags(
          $content['#cache']['tags'] ?? [],
          $viewRenderable['#cache']['tags'] ?? []
        );
      }
    }
  }

}
