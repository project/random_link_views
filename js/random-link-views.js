/**
 * @file
 * Random link from views behaviors.
 */

(function (Drupal, drupalSettings) {

  'use strict';

  /**
   * Search for elements to perform randoms replacements.
   */
  Drupal.behaviors.randomLinkViews = {
    attach: function (context, settings) {
      drupalSettings.randomLinkViews.forEach((randomView) => {
        const pick = randomView.randomSet[Math.floor(Math.random() * randomView.randomSet.length)];
        const selector = randomView.selector === '' ? pick.selector : randomView.selector;
        const attribute = randomView.attribute === '' ? pick.attribute : randomView.attribute;
        const replacement = randomView.replacement === '' ? pick.replacement : randomView.replacement;

        context.querySelectorAll(selector).forEach((element) => {
          element[attribute] = replacement;
        });

      });
    }
  };

} (Drupal, drupalSettings));
